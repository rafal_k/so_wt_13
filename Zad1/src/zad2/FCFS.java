/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zad2;

/**
 *
 * @author Rafał
 */
public class FCFS implements Algorithm {
    
    public Task handle(TaskQueue queue, Disc disk){
        if(queue.size()==0) return null;
        int min=queue.get(0).arrivalTime;
        Task minTask=queue.get(0);
        for(int i=0;i<queue.size();i++) {
            if(queue.get(i).arrivalTime<min) {
                min=queue.get(i).arrivalTime;
                minTask=queue.get(i);
            }
        }
        if (minTask.isHasArrived()) return minTask;
        return null;
    }
}