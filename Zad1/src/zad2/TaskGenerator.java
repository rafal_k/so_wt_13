package zad2;

import java.util.ArrayList;
import java.util.Random;

public class TaskGenerator
{
    public static TaskQueue generate(Disc d, int taskLimit, int maxWaitingTime) {
    	// ArrayList<Task> ar = new ArrayList<>();
    	TaskQueue q = new TaskQueue();
    	for (int i=0; i<taskLimit; i++) {
    		int blockId = getRandomBlockId(d.size);
    		int arrivalTime = getRandomArrivalTime(maxWaitingTime);
            Task t = new Task(blockId, arrivalTime, false);
            q.add(t);
    	}
        return q;
    }
    
    public static void testGeneratedTasks(Disc d, int taskLimit, int maxWaitingTime) {
    	TaskQueue q = new TaskQueue();
    	for (int i=0; i<taskLimit; i++) {
    		int blockId = getRandomBlockId(d.size);
    		int arrivalTime = getRandomArrivalTime(maxWaitingTime);
            Task t = new Task(blockId, arrivalTime, false);
            q.add(t);
    	}
    	for (int i=0; i<q.size(); i++) {
    		System.out.println(q.get(i));
    	}
    }
    
    public static TaskQueue generateTestTasks() {
    	TaskQueue q = new TaskQueue();
    	Task t1 = new Task(100, 3, false);
    	Task t2 = new Task(50, 31, false);
    	Task t3 = new Task(15, 81, false);
    	Task t4 = new Task(87, 39, false);
    	Task t5 = new Task(4, 12, false);
    	Task t6 = new Task(66, 96, false);
    	q.push(t1);
    	q.push(t2);
    	q.push(t3);
    	q.push(t4);
    	q.push(t5);
    	q.push(t6);
    	return q;
    }
    
    public static int getRandomBlockId(int maxBlockId) {
			Random r = new Random();
   			return r.nextInt((maxBlockId) + 1);
    }
    
    public static int getRandomArrivalTime(int maxArrivalTime) {
    	Random r = new Random();
		return r.nextInt((maxArrivalTime) + 1);
    }
    
}
