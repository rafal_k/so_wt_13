package zad2;

public class Disc {
    int size;
    int currentPosition;
    
    public Disc(int size)
    {
       this.size = size;
       currentPosition = 0;
    }
    
    public int getCurrentPosition()
    {
        return currentPosition;
    }
    
    public void moveToPosition(int position)
    {
        currentPosition = position;
    }
}
