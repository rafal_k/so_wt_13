package zad2;

public class Task
{
    int blockId;
    int arrivalTime;
    boolean hasArrived;
    
    public String toString() {
    	String s = Integer.toString(blockId) + " ; " + Integer.toString(arrivalTime);
    	return s;
    }
    
    public Task(int blockId, int arrivalTime, boolean hasArrived) {
        this.blockId = blockId;
        this.arrivalTime = arrivalTime;
        this.hasArrived = hasArrived;
    }
    
    public Task(int blockId, int arrivalTime) {
        this.blockId = blockId;
        this.arrivalTime = arrivalTime;
        this.hasArrived = false;
    }

    public int getBlockId() {
        return blockId;
    }

    public void setBlockId(int blockId) {
        this.blockId = blockId;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public boolean isHasArrived() {
		return hasArrived;
	}
    
    public boolean hasArrived() {
		return hasArrived;
	}

	public void setHasArrived(boolean hasArrived) {
		this.hasArrived = hasArrived;
	}

	public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
    
    
    
}
