
package zad2;

import java.util.ArrayList;

public class TaskQueue {

	ArrayList<Task> ar = new ArrayList<>();

	public void push(Task t) {
		ar.add(t);
	}

	public void add(Task t) {
		ar.add(t);
	}

	public void remove(Task t) {
		ar.remove(t);
	}

	public Task get(int i) {
		return ar.get(i);
	}

	public Task peek() {
		return ar.get(0);
	}

	public Task pop() {
		Task t = ar.get(0);
		ar.remove(0);
		return t;
	}

	public int size() {
		return ar.size();
	}

	public boolean isEmpty() {
		return ar.isEmpty();
	}

	/**
	 * @todo przekazać kolekcję (arrayList(?)) obiektów klasy Task
	 */
	public TaskQueue() {

	}

	/*
	 * public Task getNext() { return new Task(1,2); }
	 */
}
