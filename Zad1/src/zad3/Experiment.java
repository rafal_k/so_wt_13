package zad3;

import java.util.ArrayList;
import java.util.List;

class Experiment {
	private int currentTime;
	private List<Frame> frameList;
	private List<Request> requestList;
	private Algorithm algorithm;
	private int pageFaultNumber;
	
	public Experiment(List<Request> requestList, int framesQuantity, Algorithm algorithm) {
		currentTime = 0;
		frameList = new ArrayList<Frame>();
		for(int i =0; i < framesQuantity; i++) {
			frameList.add(new Frame());
		}
		this.requestList = requestList;
		this.algorithm = algorithm;
		pageFaultNumber = 0;
	}
	
	//return noumber of page faults
	public int run() { 
		while(!requestList.isEmpty()) {
			if(algorithm.execute(requestList, frameList, currentTime)) {
				pageFaultNumber++;
			}
			currentTime++;
			updateHasArrived();
		}
		return pageFaultNumber;
	}
	
	private void updateHasArrived() {
		for(int i = 0; i < requestList.size(); i++) {
			if(requestList.get(i).arrivalTime <= currentTime) {
				requestList.get(i).setHasArrived(true);
			}
		}
	}
}
