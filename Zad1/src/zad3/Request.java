package zad3;

public class Request implements Comparable<Request> {
	int pageNumber;
	int arrivalTime;
	boolean hasArrived;
	
	public Request(int pageNumber, int arrivalTime, boolean hasArrived) {
		super();
		this.pageNumber = pageNumber;
		this.arrivalTime = arrivalTime;
		this.hasArrived = hasArrived;
	}
	
	public Request(int pageNumber, int arrivalTime) {
		this.pageNumber = pageNumber;
		this.arrivalTime = arrivalTime;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public boolean isHasArrived() {
		return hasArrived;
	}

	public void setHasArrived(boolean hasArrived) {
		this.hasArrived = hasArrived;
	}
	
	public String toString() {
		String s = "pageNumber= " + pageNumber + " ; arrivalTime= " + arrivalTime;
		return s;
	}
	
	public int compareTo(Request anotherRequest) {
		return (arrivalTime - anotherRequest.getArrivalTime());
	}
	
	
	
	
	
	
}
