package zad3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class RequestGenerator {

	static Random r = new Random();

	public List<Request> generateRequestList(int mainMemorySize, int virtualMemorySize, int maxArrivalTime,
			int numberOfRequests) {

		ArrayList<Request> requests = new ArrayList<>();
		for (int i = 0; i < numberOfRequests; i++) {
			int arrivalTime = getRandomArrivalTime(maxArrivalTime);
			int pageNumber = getRandomPageNumber(virtualMemorySize);
			Request newRequest = new Request(pageNumber, virtualMemorySize);
			requests.add(newRequest);
		}
		
		Collections.sort(requests);
		return requests;
	}

	public List<Request> generateRequestListRespectingLocality(int mainMemorySize, int virtualMemorySize,
			int maxArrivalTime, int numberOfRequests) {
		ArrayList<Request> requests = new ArrayList<>();
		int lastPageNumber = virtualMemorySize / 2;
		for (int i = 0; i < numberOfRequests; i++) {
			int arrivalTime = getRandomArrivalTime(maxArrivalTime);
			// int pageNumber = getRandomPageNumber(virtualMemorySize);
			int pageNumber = getRandomPageNumberRespectingLocality(virtualMemorySize, lastPageNumber);
			lastPageNumber = pageNumber;
			Request newRequest = new Request(pageNumber, virtualMemorySize);
			requests.add(newRequest);
		}
		Collections.sort(requests);
		return requests;
	}

	public static int getRandomArrivalTime(int maxArrivalTime) {
		return r.nextInt((maxArrivalTime) + 1);
	}

	public static int getRandomPageNumber(int virtualMemorySize) {
		return r.nextInt((virtualMemorySize) + 1);
	}

	public static int getRandomPageNumberRespectingLocality(int virtualMemorySize, int previousPageNumber) {
		int returnVal;
		do {
			int randomPageSizeFromInterval = r.nextInt((virtualMemorySize) + 1);
			int spread = randomPageSizeFromInterval / 5;
			int plusMinus = r.nextInt(2);
			System.out.println(plusMinus);
			if (plusMinus == 0) {
				plusMinus = -1;
			}
			returnVal = previousPageNumber + plusMinus * spread;
		} 
		while (returnVal >= 0 && returnVal < virtualMemorySize);
		return returnVal;
	}

/*	public static List generateTestRequestList() {
		ArrayList<Request> testList = generate
	}*/

}
