package zad3;

import java.util.List;

public interface Algorithm {
	
	// true - blad strony
	boolean execute(List<Request> requestList, List<Frame> frameList, int currentTime);

}
