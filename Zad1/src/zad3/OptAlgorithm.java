package zad3;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OptAlgorithm implements Algorithm{

    private Frame findFrame(List<Frame> frameList, int pageNumber){
        for (Frame frame : frameList) {
            if(frame.pageReferenced == pageNumber) return frame;
        }
        return null;
    }

    private Frame findLastReferencedPage(List <Request> requestList, List<Frame> frameList, int currentTime){
        Integer framesMatched = 0;
        Map<Frame, Integer> closestReferences = new HashMap<>();
        frameList.forEach(frame -> closestReferences.put(frame, -1));

        for (int i = 1; i < requestList.size(); i++){
            //!!! if(frameList.equals(frameList.size())) break;
            Request currentRequest = requestList.get(i);
            Frame currentFrame = findFrame(frameList, currentRequest.pageNumber);
            if(currentFrame == null) continue;

            if(closestReferences.get(currentFrame).equals(-1)) continue;
            closestReferences.put(currentFrame, i);
            framesMatched++;
        }


    }

    //returns true when requested page is not in frame list
    @Override
    public boolean execute(List<Request> requestList, List<Frame> frameList, int currentTime) {
        if(requestList.isEmpty()) return false;
        Request firstRequest = requestList.get(0);
        if(frameList.contains(firstRequest.pageNumber)) return false;

        for (Frame frame : frameList) {
            if(frame.pageReferenced == null){
                frame.pageReferenced = firstRequest.pageNumber;
                return true;
            }
        }

        return true;
    }
}
