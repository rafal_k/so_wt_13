/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zad3;

import java.util.List;

/**
 *
 * @author stud
 */
public class FIFO implements Algorithm{

    @Override
    public boolean execute(List<Request> requestList, List<Frame> frameList, int currentTime) {
        if(!requestList.isEmpty()&&requestList.get(0).hasArrived){
            for(int i = 0; i < frameList.size(); i++){
                if(frameList.get(i).pageReferenced == null || frameList.get(i).pageReferenced == requestList.get(0).pageNumber){
                    frameList.get(i).pageReferenced = requestList.get(0).pageNumber;
                    frameList.get(i).lastAccessed = currentTime;
                    requestList.remove(0);
                    return false;
                }
            }
            int min=frameList.get(0).lastAccessed;
            int minFrame=0;
            for (int i=1;i<frameList.size();i++) {
                if (frameList.get(i).lastAccessed<min) {
                    min=frameList.get(i).lastAccessed;
                    minFrame=i;
                }    
            }
            frameList.get(minFrame).lastAccessed=currentTime;
            frameList.get(minFrame).pageReferenced=requestList.get(0).pageNumber;
            requestList.remove(0);
            return true;
        }
        return false;
    }
    
}
