package zad3;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

	public static void main(String[] args) {
		Map<String, Algorithm> map = new HashMap<>();
		map.put("FIFO", new FIFO());
		List<Request> requestList = (new RequestGenerator()).generateRequestList(1000, 2000, 5000, 10000);
		for(Map.Entry<String, Algorithm> entry : map.entrySet()) {
			Experiment currentExperiment = new Experiment(requestList, 1000, entry.getValue());
			System.out.print("Running " + entry.getKey() + ": " + currentExperiment.run());
		}
	}

}
