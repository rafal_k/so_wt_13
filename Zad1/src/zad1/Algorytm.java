import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;



public class Algorytm {
    public static void main(String[] args) {
        // System.out.println(344);
        ArrayList<Process> ar1 = new ArrayList<>();
        ar1.add(new Process(2, 3, 0));
        ar1.add(new Process(4, 7, 1));
        ar1.add(new Process(8, 2, 2));
        // ar1.add(new Process(9, 3, 3));

//        ar1.add(new Process(2, 5, 0));
//        ar1.add(new Process(3, 8, 1));
//        ar1.add(new Process(4, 3, 2));

//        ar1.add(new Process(2, 7, 0));
//        ar1.add(new Process(4, 2, 1));
//        ar1.add(new Process(7, 10, 2));

//        ar1.add(new Process(2, 3, 0));
//        ar1.add(new Process(3, 4, 1));

//        ArrayList<Process> testData = Process.generateTestData(100);
//        Process.printArray(testData);

        Algorytm a = new Algorytm();
        a.fcfs(ar1);
        System.out.println(564);
        // a.fcfs(testData);
        //a.sjf(testData);
//        a.sjfw(testData);
//        a.roundrobin(testData, 2);

        // a.fcfs(ar1);
//        a.sjf(ar1);
//        a.sjfw(ar1);
//        a.roundrobin(ar1, 2);
    }

    public LinkedList<Process> fcfs(ArrayList<Process> processes){
        long timer = 0;
        LinkedList<Process> nextToExec = new LinkedList<>();
        LinkedList<Process> finishedProcesses = new LinkedList<>();
        Process currentlyExecuting = null;

        while (!processes.isEmpty() || !nextToExec.isEmpty() || currentlyExecuting != null){
            LinkedList<Process> newlyAppeared = getAllNewProcesses(processes, timer);
            for (Process p : newlyAppeared){
                nextToExec.add(p);
            }
            if (currentlyExecuting != null){
                currentlyExecuting.runningTime += 1;
                if (currentlyExecuting.runningTime == currentlyExecuting.execTime){
                    currentlyExecuting.setFinishTime(timer);
                    System.out.println("finished");
                    finishedProcesses.add(currentlyExecuting);
                    currentlyExecuting = null;
                }
            }
            if (currentlyExecuting == null && !nextToExec.isEmpty()){
                currentlyExecuting = nextToExec.getFirst();
                nextToExec.remove(0);
            }
            timer++;
        }
        System.out.println("size= " + finishedProcesses.size());
        for (Process p : finishedProcesses){
            System.out.println(p.getFinishTime());
        }

        return finishedProcesses;

    }

    public LinkedList<Process> sjf(ArrayList<Process> processes){
        long timer = 0;
        LinkedList<Process> nextToExec = new LinkedList<>();
        LinkedList<Process> finishedProcesses = new LinkedList<>();
        Process currentlyExecuting = null;

        while (!processes.isEmpty() || !nextToExec.isEmpty() || currentlyExecuting != null){
            LinkedList<Process> newlyAppeared = getAllNewProcesses(processes, timer);
            for (Process p : newlyAppeared){
                nextToExec.add(p);
            }
            if (currentlyExecuting != null){
                currentlyExecuting.runningTime += 1;
                if (currentlyExecuting.runningTime == currentlyExecuting.execTime){
                    currentlyExecuting.setFinishTime(timer);
                    System.out.println("finished");
                    finishedProcesses.add(currentlyExecuting);
                    currentlyExecuting = null;
                }
            }
            if (currentlyExecuting == null && !nextToExec.isEmpty()){
                currentlyExecuting = getShortestProcess(nextToExec);
                nextToExec.remove(currentlyExecuting);
            }
            timer++;
        }
        System.out.println("size= " + finishedProcesses.size());
        for (Process p : finishedProcesses){
            System.out.println(p.getFinishTime());
        }

        return finishedProcesses;
    }


    public LinkedList<Process> sjfw(ArrayList<Process> processes){
        long timer = 0;
        LinkedList<Process> nextToExec = new LinkedList<>();
        LinkedList<Process> finishedProcesses = new LinkedList<>();
        Process currentlyExecuting = null;

        while (!processes.isEmpty() || !nextToExec.isEmpty() || currentlyExecuting != null){
            LinkedList<Process> newlyAppeared = getAllNewProcesses(processes, timer);
            for (Process p : newlyAppeared){
                nextToExec.add(p);
            }
            if (currentlyExecuting != null){

                Process potentialNewProcess = getLeastRemainingTimeProcess(nextToExec);
                if (currentlyExecuting.getRemainingTime() > potentialNewProcess.getRemainingTime()){
                    nextToExec.add(currentlyExecuting);
                    currentlyExecuting = potentialNewProcess;
                    nextToExec.remove(currentlyExecuting);
                }

//                Process shortest = getShortestProcess(nextToExec);
//                if (shortest.getExecTime())

                currentlyExecuting.runningTime += 1;
                if (currentlyExecuting.runningTime == currentlyExecuting.execTime){
                    currentlyExecuting.setFinishTime(timer);
                    System.out.println("finished");
                    finishedProcesses.add(currentlyExecuting);
                    currentlyExecuting = null;
                }
            }
            if (currentlyExecuting == null && !nextToExec.isEmpty()){
                currentlyExecuting = getLeastRemainingTimeProcess(nextToExec);
                nextToExec.remove(currentlyExecuting);
            }
            timer++;
        }
        System.out.println("size= " + finishedProcesses.size());
        for (Process p : finishedProcesses){
            System.out.println("id: " + p.getId() + "; finishTime: " + p.getFinishTime());
        }

        return finishedProcesses;
    }


    public LinkedList<Process> roundrobin(ArrayList<Process> processes, int timeQuantuum){
        long timer = 0;
        LinkedList<Process> nextToExec = new LinkedList<>();
        LinkedList<Process> finishedProcesses = new LinkedList<>();
        Process currentlyExecuting = null;

        while (!processes.isEmpty() || !nextToExec.isEmpty() || currentlyExecuting != null){
            LinkedList<Process> newlyAppeared = getAllNewProcesses(processes, timer);
            for (Process p : newlyAppeared){
                nextToExec.add(p);
            }
            if (currentlyExecuting != null){


//                Process potentialNewProcess = getLeastRemainingTimeProcess(nextToExec);
//                if (currentlyExecuting.getRemainingTime() > potentialNewProcess.getRemainingTime()){
//                    nextToExec.add(currentlyExecuting);
//                    currentlyExecuting = potentialNewProcess;
//                    nextToExec.remove(currentlyExecuting);
//                }


//                Process shortest = getShortestProcess(nextToExec);
//                if (shortest.getExecTime())

                currentlyExecuting.runningTimeInThisTimeQuantuum += 1;
                currentlyExecuting.runningTime += 1;
                if (currentlyExecuting.runningTime == currentlyExecuting.execTime){
                    currentlyExecuting.setFinishTime(timer);
                    System.out.println("finished");
                    finishedProcesses.add(currentlyExecuting);
                    currentlyExecuting = null;
                }

                if ((currentlyExecuting != null) && (currentlyExecuting.getRunningTimeInThisTimeQuantuum() >= timeQuantuum)){
                    currentlyExecuting.setRunningTimeInThisTimeQuantuum(0);
                    nextToExec.add(currentlyExecuting);
                    currentlyExecuting = nextToExec.getFirst();
                    nextToExec.removeFirst();
                }

            }
            if (currentlyExecuting == null && !nextToExec.isEmpty()){
                currentlyExecuting = nextToExec.getFirst();
                // nextToExec.remove(currentlyExecuting);
                nextToExec.removeFirst();
            }
            timer++;
        }
        System.out.println("size= " + finishedProcesses.size());
        for (Process p : finishedProcesses){
            System.out.println("id: " + p.getId() + "; finishTime: " + p.getFinishTime());
        }

        return finishedProcesses;
    }


    public Process getShortestProcess(LinkedList<Process> processes){
        Process shortest = new Process(-1, 999999, -1); // dummy process
        for (Process p : processes){
            if (p.getExecTime() < shortest.getExecTime()){
                shortest = p;
            }
        }
        return shortest;
    }

    public Process getLeastRemainingTimeProcess(LinkedList<Process> processes){
        Process leastRemainingTimeProcess = new Process(-1, 999999, -1); // dummy process
        for (Process p : processes){
            if ((p.getExecTime()-p.getRunningTime()) < (leastRemainingTimeProcess.getExecTime() - leastRemainingTimeProcess.getRunningTime())){
                leastRemainingTimeProcess = p;
            }
        }
        return leastRemainingTimeProcess;
    }

    public LinkedList<Process> getAllNewProcesses(ArrayList<Process> processes, long timer){
        LinkedList<Process> ar = new LinkedList<>();
        ArrayList<Integer> indexesToRemove = new ArrayList<Integer>();
        for (int i=0; i<processes.size(); i++){
            if (processes.get(i).getArrivalTime() == timer){
                ar.add(processes.get(i));
                // indexesToRemove.add(i);
                processes.remove(i);
            }
        }

//        for (int i=indexesToRemove.size()-1; i>=0; i--){
//            processes.remove(indexesToRemove.get(i));
//        }

//        for (Process p : processes){
//            if (p.getArrivalTime() == timer){
//                ar.add(p);
//            }
//        }
        return ar;
    }

}

//
//class Process {
//    long arrivalTime;
//    long execTime;
//    long finishTime = -1;
//    long runningTime = 0;
//    int id;
//    long runningTimeInThisTimeQuantuum = 0;
//
//    public Process(long arrivalTime, long execTime, int id){
//        this.arrivalTime = arrivalTime;
//        this.execTime = execTime;
//        this.id = id;
//        runningTime = 0;
//    }
//
//    public long getArrivalTime() {
//        return arrivalTime;
//    }
//
//    public void setArrivalTime(long arrivalTime) {
//        this.arrivalTime = arrivalTime;
//    }
//
//    public long getExecTime() {
//        return execTime;
//    }
//
//    public void setExecTime(long execTime) {
//        this.execTime = execTime;
//    }
//
//    public long getFinishTime() {
//        return finishTime;
//    }
//
//    public void setFinishTime(long finishTime) {
//        this.finishTime = finishTime;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public long getRunningTime() {
//        return runningTime;
//    }
//
//    public void setRunningTime(long runningTime) {
//        this.runningTime = runningTime;
//    }
//
//    public long getRemainingTime(){
//        return getExecTime() - getRunningTime();
//    }
//
//    public long getRunningTimeInThisTimeQuantuum() {
//        return runningTimeInThisTimeQuantuum;
//    }
//
//    public void setRunningTimeInThisTimeQuantuum(long runningTimeInThisTimeQuantuum) {
//        this.runningTimeInThisTimeQuantuum = runningTimeInThisTimeQuantuum;
//    }
//
//    public static ArrayList<Process> generateTestData(int len){
//        ArrayList<Process> ar = new ArrayList<>();
//        Random rand = new Random();
//        for (int i=0; i<len; i++){
//            long arrivalTime = (long)Math.abs(rand.nextGaussian() * 100 + 20);
//            long execTime = (long)Math.abs(rand.nextGaussian() * 5 + 5) + 1;
//            int id = i;
//            ar.add(new Process(arrivalTime, execTime, id));
//        }
//        return ar;
//    }
//
//    public static void printArray(ArrayList<Process> ar){
//        for (Process p : ar){
//            System.out.println(p);
//        }
//        System.out.println("");
//    }
//
//    public String toString(){
//        String s = "id= " + getId() + "; arrivalTime= " + getArrivalTime() + "; execTime=" + getExecTime();
//        return s;
//    }
//
//} // test
