/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zad1;
import java.util.*;
/**
 *
 * @author stud
 */
public class ProcessesQueue{
    private ArrayList<Process> queue;
    
    public ProcessesQueue(){
        queue = new ArrayList<Process>();
    }
    
    public void enqueue(Process p){
        queue.add(p);
    }
    
    public boolean isEmpty(){
        return queue.size() == 0;
    }
    
    public Process dequeue(){
        if (this.isEmpty())
                return null;
        return queue.remove(0);
    }
}
