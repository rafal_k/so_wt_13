package zad1.experiment;

import zad1.ProcessesQueueGenerator;

import java.util.ArrayList;
import java.util.LinkedList;

public class Experiment {

    public ArrayList <Long> getTimes(LinkedList<Process> processes){
        ArrayList <Long> times = new ArrayList<>();
        for (Process p :
                processes) {
            long time = 0;
            times.add(time);
        }
        return times;
    }

    public double average(ArrayList<Long> times){
        long totalTime = 0;
        for (Long time :
                times) {
            totalTime+=time;
        }
        return totalTime/times.size();
    }

    public void doExperiment()
}
