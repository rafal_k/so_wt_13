/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zad1;
import java.util.*;

/**
 *
 * @author stud
 */
public class Process implements Comparable<Process>{
   private int id;
   private int arrival_time;
   private int time_total;
   public int running_time;
   private boolean isDone;
   
   public Process(int id, int arrival_time, int time_total){
       this.id = id;
       this.arrival_time = arrival_time;
       this.time_total = time_total;
       this.isDone = false;
       this.running_time = 0;
   }
    
   public int getId(){
       return id;
   }
   public int getArrival_time(){
       return arrival_time;
   }
   public int getTime_total(){
       return time_total;
   }
   public boolean getIsDone(){
       return isDone;
   }  
   public void endProcess(){
       this.isDone = true;
   }

    @Override
    public int compareTo(Process p) {
        return this.running_time - p.running_time;
    }
    
}
