/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zad1;

import java.util.Random;

/**
 *
 * @author Grupa Test-Data
 */
public class ProcessesQueueGenerator {

    /**
     * @param median is an expected median of the time the processes are expected to take.
     * @param var is a expected maximum variancy of the time.
     * @param processQuantity is an amount of the processes to be generated.
     *
     */
    public static ProcessesQueue generate(int median, double var, int processQuantity) {
        Random random = new Random();
        ProcessesQueue procesessQueue = new ProcessesQueue();
        int processRegistrationTime = 0;
        int totalProcessTime = 0;
        
        for(int i = 0; i < processQuantity; i++) {
            int index = i;
            int processTime = median + (int)((random.nextGaussian() - 0.5)*var);
            procesessQueue.enqueue(new Process(index, processTime, processRegistrationTime));
            processRegistrationTime += random.nextInt(totalProcessTime - processRegistrationTime) + processRegistrationTime;
        }
        
        return procesessQueue;
    }
}
